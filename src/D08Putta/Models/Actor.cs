﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace D08Putta.Models
{
    public class Actor
    {
        public int ActorID { get; set; }
  
        public string LastName { get; set; }
        
        public string FirstName { get; set; }

        public string DateOfBirth { get; set; }

        public string Languages { get; set; }

        public int NumberOfMovies { get; set; }

        public int YearsOfExperience { get; set; }

        public double Remuneration { get; set; }

        public string Achievements { get; set; }

        public string Qualification { get; set; }

        public int Height { get; set; }

        public double Weight { get; set; }

        public int Rating { get; set; }
    }
}
