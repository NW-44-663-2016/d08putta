using System;
using Microsoft.Data.Entity;
using Microsoft.Data.Entity.Infrastructure;
using Microsoft.Data.Entity.Metadata;
using Microsoft.Data.Entity.Migrations;
using D08Putta.Models;

namespace D08Putta.Migrations
{
    [DbContext(typeof(ApplicationDbContext))]
    partial class ApplicationDbContextModelSnapshot : ModelSnapshot
    {
        protected override void BuildModel(ModelBuilder modelBuilder)
        {
            modelBuilder
                .HasAnnotation("ProductVersion", "7.0.0-rc1-16348")
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("D08Putta.Models.Actor", b =>
                {
                    b.Property<int>("ActorID")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Achievements");

                    b.Property<string>("DateOfBirth");

                    b.Property<string>("FirstName");

                    b.Property<int>("Height");

                    b.Property<string>("Languages");

                    b.Property<string>("LastName");

                    b.Property<int>("NumberOfMovies");

                    b.Property<string>("Qualification");

                    b.Property<int>("Rating");

                    b.Property<double>("Remuneration");

                    b.Property<double>("Weight");

                    b.Property<int>("YearsOfExperience");

                    b.HasKey("ActorID");
                });
        }
    }
}
